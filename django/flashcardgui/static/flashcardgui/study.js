var cards;
var idx;
var result;
var post_results = [];
var current_card;
var result_list = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0};
var random = false;
var card_count = 0;
var clock;
var finished=false
var secs=5;
var counter
$body = $("body");



$(document).on({
    ajaxStart: function () {
        $body.addClass("loading");
    },
    ajaxStop: function () {
        $body.removeClass("loading");
        if(finished==false){

        }


    }
});


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


answer_bs = '<button type="button" class="btn btn-primary" id="answer-button">Answer</button>';
result_bs = '<button type="button" class="btn btn-primary" id="result-0">0</button> \
             <button type="button" class="btn btn-primary" id="result-1">1</button> \
             <button type="button" class="btn btn-primary" id="result-2">2</button> \
             <button type="button" class="btn btn-primary" id="result-3">3</button> \
             <button type="button" class="btn btn-primary" id="result-4">4</button> \
             <button type="button" class="btn btn-primary" id="result-5">5</button>';


function congrat() {
    finished = true;
    $('#status').text('0');
    $('#result-row').hide();
    if (random) {
        $('#question').text("Tebrikler!");

    } else {
        $('#question').text("Tebrikler! Başka Soru Kalmadı");

    }
    $('#reset-button').show();
    $('#answer').hide();


    var counts = {};
    datas = $.map(result_list, function (value, key) {
        return value
    });
    labels = Object.keys(result_list);
    data = {
        "labels": labels,
        "datasets": [{
            "data": datas,
            backgroundColor: ["#000000", "#8e5ea2", "#e8c3b9", "#c45850", "#3e95cd", "#3cba9f"],

        }]
    };

    var ctx = document.getElementById('chart').getContext('2d');
    var chart = new Chart(ctx, {
        "type": "doughnut",
        "data": data,
        'options': {
            cutoutPercentage: 50,
        }

    });

}


function get_cards() {
    $.ajax({
        url: 'get_cards/',
        success: function (data) {
            console.log(data);
            cards = data;
            console.log(cards.counts);


            post_results.length = 0;
            if (cards.cards.length) {
                current_card = cards.cards.length - 1;
                show_card(current_card);
                if (cards.cards[0].deck_id === 'random') {
                    random = true;
                }
            } else {
                $('#countdown').hide();
                congrat();
            }
        }
    });
}

function show_card(num) {
    console.log(num);
    $('#status').text(cards.count);
    $('#question').html(cards.cards[num].question);
    $('#answer').html('<hr>' + cards.cards[num].answer);
    $('#answer').hide();

    $('#result-row').html(answer_bs);
    $('#answer-button').click(show_answer)
    secs = 5
    counter=setInterval(timer, 1000);
    progress(secs, secs, $('#countdown'));



    function push_but(event) {
        <!-- alert(event.target.id) -->
        switch (event.target.id) {
            case 'result-0':
                result = 0;
                break;
            case 'result-1':
                result = 1;
                break;
            case 'result-2':
                result = 2;
                break;
            case 'result-3':
                result = 3;
                break;
            case 'result-4':
                result = 5;
                break;
            case 'result-5':
                result = 5;
                break;
        }
        if (result > 2) {
            cards.count--;
        }

        post_results.push({
            'id': cards.cards[current_card].id,
            'result': result
        });
        console.log(post_results);
        result_list[result]++
        next_card();
    }

    function show_answer() {
        clearInterval(counter);

        $('#answer').show();
        $('#result-row').html(result_bs);
        $('#result-0').click(push_but);
        $('#result-1').click(push_but);
        $('#result-2').click(push_but);
        $('#result-3').click(push_but);
        $('#result-4').click(push_but);
        $('#result-5').click(push_but);

    }


    function next_card() {

        if (random) {
            card_count++
            if (card_count >= 50) {
                return congrat();
            }
        }
        if (current_card) {
            current_card--;
            show_card(current_card);
        } else {
            console.log(JSON.stringify(post_results));
            $.ajax({
                url: "get_cards/",
                type: "POST",
                dataType: "json",
                data: JSON.stringify(post_results),
                success: function (result) {
                    console.log(result);
                    get_cards();
                }
            });
        }
    }

function get_clock() {
    $('#countdown').show();


    clock = new FlipClock($('#countdown'), 5, {
        clockFace: 'Counter',
        countdown: true,
        autoStart: true,
        minimumDigits:1,
        language: 'tr',
        callbacks: {
            stop: function () {

                show_answer();

            }
        }
    });
}

function timer()
{

  if (secs <= 0)
  {


     show_answer();
     clearInterval(counter);

return;
  }
    secs=secs-1;


}
}

function progress(timeleft, timetotal, $element) {
    var progressBarWidth = timeleft * $element.width() / timetotal;
    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, 'linear').html(timeleft);
    if(timeleft > 0) {
        setTimeout(function() {
            progress(timeleft - 1, timetotal, $element);
        }, 1000);
    }
};

console.log("1");
get_cards();

function bind_video(){
        	$(document).on( "click", ".vid_modal", function(e){

		var newW = Number($(this).attr('data-w')) +40;
		var newH = Number($(this).attr('data-h')) +40;

		$("#modal_title").html('');

		$(".modal-dialog").width(  newW +'px' );
		$(".modal-dialog").height( newH +'px' );

		$("#modal_video").width( $(this).attr('data-w') +'px' );
		$("#modal_video").height( $(this).attr('data-h') +'px' );
		$("#modal_video").attr('src', $(this).attr('data-target') );
		$('#videoModal').modal('show');

		$("#modal_title").html( $(this).attr('data-from') );
	});
}