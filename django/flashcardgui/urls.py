from django.conf.urls import url
from flashcardgui import views


urlpatterns = [
    url('^$', views.home, name='home'),
    url('^about/$', views.about, name='about'),
    url('^profile/$', views.profile, name='flashcardgui.views.profile'),
    url('^add/$', views.add, name='add-card'),
    url('^add/newdeck$', views.add_new, name='add-card-deck'),
    url('^add/batch', views.add_batch, name='add-card-batch'),
    url('^study/(?P<deck_id>[0-9]+)/$', views.study, name='study'),
    url('^study/random/$', views.study_random, name='study_random'),
    url('^study/(?P<deck_id>\w+)/get_cards/$', views.get_cards, name='get-cards'),
    url('^study/(?P<deck_id>\w+)/reset_deck/$', views.reset_deck, name='get-reset_deck'),
    url('^delete_deck/$', views.delete_deck, name='delete_deck'),
    url('^rename_deck/$', views.rename_deck, name='rename_deck'),

    url('^post_results/$', views.post_results, name='post_results'),

]
