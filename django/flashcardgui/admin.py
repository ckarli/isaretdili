from django.contrib import admin
from api.models import Deck, UserDeck, Flashcard


class DeckAdmin(admin.ModelAdmin):
        pass


class FlashcardAdmin(admin.ModelAdmin):
        search_fields = ('question', )


class UserDeckAdmin(admin.ModelAdmin):
        list_filter = ('owner',)

admin.site.register(Deck, DeckAdmin)
admin.site.register(UserDeck, UserDeckAdmin)
admin.site.register(Flashcard, FlashcardAdmin)
