from django import template
from api.models import Flashcard
register = template.Library()

@register.simple_tag(takes_context=True)
def get_card_num(context,deck):
    request = context['request']
    cards = Flashcard.objects.get_cards_to_study(user=request.user, deck_id=deck.id, days=0)
    return len(cards)