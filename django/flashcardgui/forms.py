from django import forms

from api.models import Deck


class CardForm(forms.Form):
    deck = forms.CharField(label='Deck', max_length=100)
    question = forms.CharField(label='Front', widget=forms.Textarea)

    def __init__(self, user, *args, **kwargs):
        super(CardForm, self).__init__(*args, **kwargs)
        self.fields['deck'].required = False
        decks= Deck.objects.filter(owner=user)
        self.fields['deck'].choices = decks.values()