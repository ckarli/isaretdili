from itertools import chain

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.utils import timezone
from django.views.decorators.csrf import ensure_csrf_cookie
from os.path import basename, splitext

import random
import json
import logging

from api.models import Deck, Flashcard, UserDeck
from flashcardgui.decorators import superuser_only
from .forms import CardForm
import requests
from bs4 import BeautifulSoup
CARD_LIMIT = 5

from raven.contrib.django.models import client

logging.basicConfig()  # you need to initialize logging, otherwise you will not see anything from requests
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True


@login_required
def home(request):
    """
    App home page
    """
    if request.method == 'GET':
        decks = Deck.objects.all()
        context = {'user': request.user, 'decks': decks}
        return render(request, 'flashcardgui/index.html', context)


def profile(request):
    """
    ...
    """
    if request.method == 'GET':
        pass


def about(request):
    """
    ...
    """
    if request.method == 'GET':
        return render(request, 'flashcardgui/about.html')


@login_required
@superuser_only
def add_batch(request):
    if request.method == 'POST':
        form = CardForm(request.user, request.POST)
        if form.is_valid():
            deck_name = form.cleaned_data['deck']
            question = form.cleaned_data['question']
            answer = ""
            user = request.user
            questions=question.splitlines()
            for question in questions:
                Flashcard.objects.create_flashcard(user=user, question=question,
                                               answer=answer, deck_name=deck_name,)
            return HttpResponseRedirect(reverse('add-card'))
    else:
        form = CardForm(request.user)

    return render(request, 'flashcardgui/add_batch.html', {'form': form,})


@login_required
@superuser_only
def add_new(request):
   return add(request,True)


@login_required
@superuser_only
def add(request,new=None):
    """
    Add new flashcard
    """
    if request.method == 'POST':
        form = CardForm(request.user, request.POST)
        if form.is_valid():
            deck_name = form.cleaned_data['deck']
            question = form.cleaned_data['question']
            #answer = form.cleaned_data['answer']
            user = request.user
            Flashcard.objects.create_flashcard(user=user, question=question,
                                               answer="", deck_name=deck_name,)
            return HttpResponseRedirect(reverse('add-card'))
    else:
        form = CardForm(request.user)

    return render(request, 'flashcardgui/add.html', {'form': form,'add_new':new})



@login_required
@ensure_csrf_cookie
def study_random(request):
    return study(request,'random')

@login_required
@ensure_csrf_cookie
def study(request, deck_id):
    """
    Study cards page (JS driven)
    """
    if request.method == 'GET':
        return render(request, 'flashcardgui/study.html',{'deck_id':deck_id})


@login_required
@ensure_csrf_cookie
def get_cards(request, deck_id):
    """
    Get cards to study (ajax)
    """

    if request.method == 'GET':
        if deck_id == 'random':
            used_cards = Flashcard.objects.filter(usercard__owner=request.user,
                                                  usercard__next_due_date__lte=timezone.now())
            new_cards = Flashcard.objects.all().exclude(usercard__owner=request.user)

        else:


            used_cards = Flashcard.objects.filter(usercard__owner=request.user, deck=deck_id,
                                             usercard__next_due_date__lte=timezone.now())
            new_cards = Flashcard.objects.filter(deck__id=deck_id).exclude(usercard__owner=request.user)


        cards = list(chain(used_cards,new_cards))
        count = len(cards)
        data = {'count': count, 'cards': []}

        num = count if count < CARD_LIMIT else CARD_LIMIT
        if num:
            # generate list of random indexes
            idx = random.sample(range(count), num)
            for i in idx:
                card = cards[i]
                question = '<p>'+'</p><p>'.join(card.question.split('\r\n'))+'</p>'
                answers=[]
                page_count = 0
                if card.answer == '' or card.modified_at+timezone.timedelta(3)<timezone.now():
                    url = "http://tidsozluk.net/tr/Sözcük/Arama/%s?"%(card.question)
                    page = requests.get(url)
                    soup = BeautifulSoup(page.text, 'html.parser')

                    first_match=soup.find('h3',string=card.question.capitalize())
                    pagination = soup.find('ul', class_='pagination')
                    if pagination:
                        page_count = len(pagination.find_all('li'))

                    if not first_match and page_count>0:
                        for x in range(2,page_count):
                            new_url = url + 'p=' +str(x)
                            page = requests.get(new_url)
                            soup = BeautifulSoup(page.text, 'html.parser')

                            match = soup.find('h3', string=card.question.capitalize())
                            if match:
                                first_match = match
                                break

                    if first_match:
                        answers.append(first_match.parent.parent.attrs.get('href'))



                if answers:
                    page = requests.get("http://tidsozluk.net%s"%answers[0])
                    soup = BeautifulSoup(page.text, 'html.parser')
                    div = soup.find_all('div', class_='container')
                    for img in div[0].findAll('img'):
                        img['src'] = "http://tidsozluk.net%s" % img['src']

                    for img in div[0].findAll('source'):
                        img['src'] = "http://tidsozluk.net%s" % img['src']

                    for vid in div[0].findAll('i',class_='vid_modal'):
                        vid['data-target'] = "http://tidsozluk.net%s" % vid['data-target']

                    answer = str(div[0])
                    card.answer = answer
                    card.save()
                else:
                    answer = '<p>'+'</p><p>'.join(card.answer.split('\r\n'))+'</p>'
                data['cards'].append({'id': card.pk, 'question': question,
                                     'answer': answer,'deck_id':deck_id})

        return JsonResponse(data)
    else:
        data = json.loads(str(request.body, 'utf-8'))
        for res in data:
            card = Flashcard.objects.get(pk=res['id'])
            userdeck,created = UserDeck.objects.get_or_create(owner=request.user, card=card      )
            userdeck.save(rating=res['result'])
        return JsonResponse({'status': 'OK'})


@login_required
@superuser_only
def delete_deck(request):
    """
    Delete deck (ajax)
    """
    if request.method == 'POST':
        data = json.loads(str(request.body, 'utf-8'))
        if 'deck_id' in data:
            deck = Deck.objects.get(owner=request.user, pk=data['deck_id'])
            deck.delete()
            return JsonResponse({'status': 'OK'})

@superuser_only
@login_required
def rename_deck(request):
    """
    Rename deck (ajax)
    """
    if request.method == 'POST':
        data = json.loads(str(request.body, 'utf-8'))
        if 'deck_id' in data and 'name' in data:
            deck = Deck.objects.get(owner=request.user, pk=data['deck_id'])
            name = data['name']
            deck.name = name
            deck.save()
            return JsonResponse({'status': 'OK'})
@login_required
def post_results(request):
    if request.method == 'POST':
        data = json.loads(str(request.body, 'utf-8'))

@login_required
def reset_deck(request, deck_id):
    if request.method == 'POST':
        data = json.loads(str(request.body, 'utf-8'))
        if 'deck_id' in data:
            deck = Deck.objects.get(owner=request.user, pk=data['deck_id'])
            usercards=request.user.usercard.filter(card__deck=deck)
            usercards.delete()
            return JsonResponse({'status': 'OK'})

