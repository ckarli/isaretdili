# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-11-11 20:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_auto_20171111_1807'),
    ]

    operations = [
        migrations.AddField(
            model_name='userdeck',
            name='rating',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
