# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-11-11 15:07
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('api', '0005_auto_20171103_0115'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserDeck',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('last_shown_at', models.DateTimeField(auto_now_add=True)),
                ('next_due_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('easiness', models.FloatField(default=2.5)),
                ('consec_correct_answers', models.IntegerField(default=0)),
            ],
        ),
        migrations.RemoveField(
            model_name='flashcard',
            name='consec_correct_answers',
        ),
        migrations.RemoveField(
            model_name='flashcard',
            name='easiness',
        ),
        migrations.RemoveField(
            model_name='flashcard',
            name='last_shown_at',
        ),
        migrations.RemoveField(
            model_name='flashcard',
            name='next_due_date',
        ),
        migrations.AddField(
            model_name='userdeck',
            name='card',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='usercard', to='api.Flashcard'),
        ),
        migrations.AddField(
            model_name='userdeck',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
