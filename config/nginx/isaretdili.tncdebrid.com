server {
    server_name www.isaretdili.info isaretdili.info;
    charset utf-8;
    client_max_body_size 100M;

    location /media  {
        alias /var/www/isaretdili/django/media;
        expires 30d;
    }

    location /static {
        alias /var/www/isaretdili/django/static;
        expires 30d;
    }

    location /robots.txt {
        alias /var/www/isaretdili/django/static/robots.txt;
    }

    location /favicon.ico {
        alias /var/www/isaretdili/django/static/favicon.ico;
          access_log     off;
          log_not_found  off;
    }

    location / {
        uwsgi_pass unix:///var/www/isaretdili/django/uwsgi.sock;
        include /etc/nginx/uwsgi_params;
    }
}
