#!/usr/bin/env bash
echo "************************"
echo "* ISARETDILI DEPLOYMENT *"
echo "************************"

echo "*******************"
echo "* PULL REPOSITORY *"
echo "*******************"
git pull

echo "************************"
echo "* BUILD DJANGO PROJECT *"
echo "************************"
sudo /home/ubuntu/.virtualenvs/env_isaretdili/bin/pip3 install -r django/requirements/production.txt

export DJANGO_READ_DOT_ENV_FILE=True
python3 django/manage.py migrate
python3 django/manage.py collectstatic --noinput
sudo touch /etc/uwsgi/sites_python3/isaretdili.ini

echo "*****************"
echo "* RESTART NGINX *"
echo "*****************"
sudo service nginx restart
sudo service nginx status

echo "*******************"
echo "* PURGE CDN CACHE *"
echo "*******************"
curl -X DELETE "https://api.cloudflare.com/client/v4/zones/a280287d7a0bb649e8fe301d4953817d/purge_cache" \
     -H "X-Auth-Email: overlord_cihat@yahoo.com" \
     -H "X-Auth-Key: 2d353badd4a6fffb13bc60304f196a4bb3a31" \
     -H "Content-Type: application/json" \
     --data '{"purge_everything":true}'

echo "***********************"
echo "* FINISHED DEPLOYMENT *"
echo "***********************"
